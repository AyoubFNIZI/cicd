const MEMBERS = ['lej', 'gowoons', 'dog'];
const cetteVariableEstInutile = 'à supprimer';

// Check if an user is a member
function isMember(login) {
  return MEMBERS.includes(login);
}

// Check if an user is a valid administrator
function adminStatus(login, password) {
  if (login === process.env.ADMIN_USERNAME && password === process.env.ADMIN_PASSWORD) {
    return { msg: `User ${login} is verified as an admin`, status: 200 };
  } else if(login === process.env.ADMIN_USERNAME) {
    return { msg: `Wrong password for ${login} user`, status: 401 };
  } else {
    return { msg: `User ${login} is not an admin`, status: 401 };
  }
  console.log("Ce texte n'est jamais affiché")
}

module.exports.isMember = isMember;
module.exports.adminStatus = adminStatus;
